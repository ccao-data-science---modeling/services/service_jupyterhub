### :warning: Development for this project has moved to a [new repository on GitHub](https://github.com/ccao-data/service-jupyterhub) :warning:

# Jupyterhub

This service runs on CCAO's Shiny server/Ubuntu VM. It runs a standard Jupyterhub instance that let's employees run notebooks on our dedicated service. The services uses [nginx](https://gitlab.com/ccao-data-science---modeling/services/service_nginx) as a reverse proxy.
